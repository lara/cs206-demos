package midterm22.appendix

// Represents optional values. Instances of Option are either an instance of
// scala.Some or the object None.
sealed abstract class Option[+A]:
  // Returns the option's value if the option is an instance of scala.Some, or
  // throws an exception if the option is None.
  def get: A
  // Returns true if the option is an instance of scala.Some, false otherwise.
  // This is equivalent to:
  //     option match
  //         case Some(v) => true
  //         case None    => false
  def isDefined: Boolean
  // Returns this scala.Option if it is nonempty, otherwise return the result of
  // evaluating alternative.
  def orElse[B >: A](alternative: => Option[B]): Option[B]

abstract class Iterable[+A]:
  // Selects all elements except first n ones.
  def drop(n: Int): Iterable[A]
  // The size of this collection.
  def size: Int
  // Selects the first n elements.
  def take(n: Int): Iterable[A]

abstract class List[+A] extends Iterable[A]:
  // Adds an element at the beginning of this list.
  def ::[B >: A](elem: B): List[B]
  // A copy of this sequence with an element appended.
  def appended[B >: A](elem: B): List[B]
  // Get the element at the specified index.
  def apply(n: Int): A
  // Selects all elements of this list which satisfy a predicate.
  def filter(pred: (A) => Boolean): List[A]
  // Selects the first element of this list.
  def head: A
  // Sorts this sequence according to a comparison function.
  def sortWith(lt: (A, A) => Boolean): List[A]
  // Selects all elements except the first.
  def tail: List[A]

abstract class Array[+A] extends Iterable[A]:
  // Get the element at the specified index.
  def apply(n: Int): A

abstract class Thread:
  // Subclasses should override this method.
  def run(): Unit
  // Causes this thread to begin execution; the Java Virtual Machine calls the
  // run method of this thread.
  def start(): Unit
  // Waits for this thread to die.
  def join(): Unit

// Creates and starts a new task ran concurrently.
def task[T](body: => T): ForkJoinTask[T] = ???

abstract class ForkJoinTask[T]:
  // Returns the result of the computation when it is done.
  def join(): T

// A concurrent hash-trie or TrieMap is a concurrent thread-safe lock-free
// implementation of a hash array mapped trie.
abstract class TrieMap[K, V]:
  // Retrieves the value which is associated with the given key. Throws a
  // NoSuchElementException if there is no mapping from the given key to a
  // value.
  def apply(key: K): V
  // Tests whether this map contains a binding for a key.
  def contains(key: K): Boolean
  // Applies a function f to all elements of this concurrent map. This function
  // iterates over a snapshot of the map.
  def foreach[U](f: ((K, V)) => U): Unit
  // Optionally returns the value associated with a key.
  def get(key: K): Option[V]
  // Collects all key of this map in an iterable collection. The result is a
  // snapshot of the values at a specific point in time.
  def keys: Iterator[K]
  // Transforms this map by applying a function to every retrieved value. This
  // returns a new map.
  def mapValues[W](f: V => W): TrieMap[K, W]
  // Associates the given key with a given value, unless the key was already
  // associated with some other value. This is an atomic operation.
  def putIfAbsent(k: K, v: V): Option[V]
  // Removes a key from this map, returning the value associated previously with
  // that key as an option.
  def remove(k: K): Option[V]
  // Removes the entry for the specified key if it's currently mapped to the
  // specified value. This is an atomic operation.
  def remove(k: K, v: V): Boolean
  // Replaces the entry for the given key only if it was previously mapped to a
  // given value. Returns true if the change is successful, or false otherwise.
  // This is an atomic operation.
  def replace(k: K, oldvalue: V, newvalue: V): Boolean
  // Adds a new key/value pair to this map.
  def update(k: K, v: V): Unit
  // Collects all values of this map in an iterable collection. The result is a
  // snapshot of the values at a specific point in time.
  def values: Iterator[V]

// An int value that may be updated atomically.
// The constructor takes the initial value at its only argument. For example,
// this create an `AtomicInteger` with an initial value of `42`:
//     val myAtomicInteger = new AtomicInteger(42)
abstract class AtomicInteger:
  // Atomically adds the given value to the current value and returns the
  // updated value.
  def addAndGet(delta: Int): Int
  // Atomically sets the value to the given updated value if the current value
  // == the expected value. Returns true if the change is successful, or false
  // otherwise. This is an atomic operation.
  def compareAndSet(oldvalue: Int, newvalue: Int): Boolean
  // Gets the current value. This is an atomic operation.
  def get(): Int
  // Atomically increments by one the current value. This is an atomic operation.
  def incrementAndGet(): Int

// ---------------------------------------------

// Needed so that we can compile successfully, but not included for students.
// See Option class doc instead.
abstract class Some[+A](value: A) extends Option[A]
object Some:
  def apply[A](value: A): Some[A] = ???
val None: Option[Nothing] = ???

object List:
  def apply[A](values: A*): List[A] = ???
val Nil: List[Nothing] = ???

object Array:
  def apply[A](values: A*): Array[A] = ???
