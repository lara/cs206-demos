// This demonstrates how to use parallel collections from a Scala worksheet.
// Note that this works because we added the "scala-parallel-collections"
// dependency in `build.sbt`. Parallel collections are not part of the Scala
// standard library anymore since Scala 2.13.

import scala.collection.parallel.immutable.ParVector
import collection.parallel.CollectionConverters.*
import collection.parallel.{ParSeq, ParSet}

val l = List(1,2,3)
val l2: ParSeq[Int] = l.par

// Calling toSet on a ParSeq returns a ParSet:
val l3: ParSet[Int] = l2.toSet
