//    PDF: https://moodle.epfl.ch/pluginfile.php/3175938/mod_folder/content/0/week01-5-First-Class-Tasks.pdf
//  Video: https://mediaspace.epfl.ch/playlist/dedicated/31866/0_c46icdbx/0_v7c766xv
// Slides: 3

package lecture1

trait Task[A]:
  def join: A

def task[A](toRun: => A): Task[A] =
  val t = threadStart(toRun)
  new Task[A]:
    override def join: A = t.joinMe
