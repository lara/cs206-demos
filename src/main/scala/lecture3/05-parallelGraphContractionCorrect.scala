//    PDF: https://moodle.epfl.ch/pluginfile.php/3175938/mod_folder/content/0/week03-4-Scala-Parallel-Collections.pdf
//  Video: https://mediaspace.epfl.ch/playlist/dedicated/31866/0_icv10qux/0_h4jt9i7k
// Slides: 31

package lecture3

import scala.collection.*
import scala.collection.parallel.CollectionConverters.MapIsParallelizable

@main def parallelGraphContractionCorrect =
  val graph =
    concurrent.TrieMap[Int, Int]() ++= (0 until 100000).map(i => (i, i + 1))
  graph(graph.size - 1) = 0
  var previous = graph.snapshot()
  for (k, v) <- graph.par do graph(k) = previous(v)
  val violation = graph.find {
    case (i, v) => v != (i + 2) % graph.size
  }
  println(s"violation: $violation")
