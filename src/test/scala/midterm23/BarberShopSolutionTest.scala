package midterm23

import instrumentation.*
import java.util.concurrent.atomic.AtomicInteger

class BarberShopSolutionTest extends munit.FunSuite:
  val implementations =
    Map[String, (Int, Scheduler) => ScheduledBarberShopSolution](
      "BarberShopSolution1" -> (new BarberShopSolution1(_)
        with ScheduledBarberShopSolution(_)),
      "BarberShopSolution2" -> (new BarberShopSolution2(_)
        with ScheduledBarberShopSolution(_)),
      "BarberShopSolution3" -> (new BarberShopSolution3(_)
        with ScheduledBarberShopSolution(_))
    )

  for (name, makeShop) <- implementations do
    for nCustomers <- 1 to 3 do
      test(f"$name with $nCustomers customer(s)") {
        testBarberShop(nCustomers, makeShop)
      }
